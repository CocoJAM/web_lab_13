# Answers to exercise 1 questions

-- departments offering courses
SELECT DISTINCT dept
FROM unidb_courses;

-- semesters being attended
SElEcT DISTINCT semester
FROM unidb_attend;

-- courses that are attended
SELECT DISTINCT c.dept,c.num
  FROM unidb_attend AS a, unidb_courses AS c
WHERE (a.dept,a.num) = (c.dept,c.num);

-- student names and country, ordered by first name
SELECT fname, lname
FROM unidb_students
ORDER BY fname ASC;

-- student names and mentors, ordered by mentors
SELECT fname, lname
FROM unidb_students
ORDER BY mentor ASC;

-- lecturers, ordered by office
SELECT *
FROM unidb_lecturers
ORDER BY office ASC;

-- staff whose staff number is greater than 500
SELECT *
FROM unidb_lecturers
WHERE staff_no> 500;

-- students whose id is greater than 1668 and less than 1824
SELECT *
FROM unidb_students
WHERE id>1668 AND id< 1824;

-- students from NZ, Australia and US
SELECT *
FROM unidb_students
WHERE country IN('NZ','AU', 'US');

-- lecturers in G Block
SELECT *
FROM unidb_lecturers
WHERE office LIKE 'G%';

-- courses not from the Computer Science Department
SELECT *
FROM unidb_courses
WHERE dept != 'comp';

-- students from France or Mexico
SELECT *
FROM unidb_students
WHERE country IN ('FR','MX');