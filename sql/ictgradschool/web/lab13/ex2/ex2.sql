# Answers to exercise 2 questions

-- names of the students who attend COMP219
SELECT s.fname, s.lname, a.num, a.dept
FROM unidb_students AS s ,unidb_attend AS a
WHERE s.id = a.id;

-- names of the student reps that are not from NZ
SELECT s.fname,s.lname
  FROM unidb_courses AS c , unidb_students AS s
WHERE c.rep_id =s.id AND s.country != 'NZ';

-- offices for the lecturers of 219
SELECT l.office
  FROM unidb_lecturers AS l , unidb_courses AS c
WHERE c.coord_no = l.staff_no AND c.num = '219';

-- names of the students taught by Te Taka
SELECT s.fname,s.lname
  FROM unidb_students AS s, unidb_lecturers AS l, unidb_courses AS c, unidb_attend AS a
WHERE c.coord_no = l.staff_no AND l.fname = 'Te Taka' AND (c.num,c.dept) = (a.num,a.dept) AND s.id = a.id;

-- students and their mentors
SELECT  concat(s.fname, ' ',s.lname) AS  Student, concat(l.fname, ' ',l.lname) AS Mentor
  From unidb_lecturers AS l ,unidb_students AS s, unidb_courses AS c
WHERE s.id = c.rep_id AND c.coord_no = l.staff_no;

-- There is something wrong below
-- lecturers whose office is in G-Block as well naming the students that are not from NZ
SELECT concat(l.fname, ' ', l.lname) AS Lecture, concat(s.fname, ' ', s.lname) AS Student
  FROM unidb_lecturers AS l, unidb_students AS s
WHERE l.office LIKE 'G%' AND s.country!= 'NZ';

-- course co-ordinator and student rep for COMP219
SELECT concat(l.fname, ' ', l.lname) AS "Co-ordinator", concat(s.fname,' ', s.lname) AS "Student rep"
  From unidb_lecturers as l , unidb_courses as c , unidb_students as s
WHERE l.staff_no = c.coord_no AND c.rep_id = s.id AND (c.num,c.dept)=(219, 'comp');